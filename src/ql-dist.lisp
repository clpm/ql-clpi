;;;; Quicklisp Distribution Representation
;;;;
;;;; This software is part of QL-CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:ql-clpi/ql-dist
    (:use #:cl
          #:alexandria
          #:anaphora
          #:clpm-multi-http-client
          #:split-sequence)
  (:import-from #:puri)
  (:export #:ql-dist-version-release
           #:ql-dist-version-release-map
           #:ql-dist-version-system-by-name-map
           #:ql-dist-version-system-by-project-map
           #:ql-dist-version-version
           #:ql-release-file-md5
           #:ql-release-prefix
           #:ql-release-size
           #:ql-release-system-files
           #:ql-release-uri
           #:ql-dist
           #:ql-dist-version
           #:ql-dist-versions
           #:ql-system-dependencies
           #:ql-system-project
           #:ql-system-system-file
           #:ql-system-system-name))

(in-package #:ql-clpi/ql-dist)

(defclass ql-dist ()
  ((http-client
    :initarg :http-client
    :documentation
    "Optional. Specify an HTTP client to use when interacting with this
distribution. If a function, it will be called with one argument. If unbound,
will fallback to *DEFAULT-HTTP-CLIENT*.")
   (uri
    :accessor ql-dist-uri)
   (force-https
    :initarg :force-https
    :initform nil
    :accessor ql-dist-force-https)
   (cache-pathname
    :initarg :cache-pathname
    :initform (error "A cache pathname must be provided")
    :accessor ql-dist-cache-pathname)
   (versions-uri
    :accessor ql-dist-versions-uri)
   (version-uris-map
    :reader ql-dist-version-uris-map)

   (dist-version-cache
    :initform (make-hash-table :test 'equal)
    :reader ql-dist-version-cache))
  (:documentation
   "Represents a Quicklisp distribution."))

(defun ql-dist-http-client (ql-dist)
  (if (slot-boundp ql-dist 'http-client)
      (let ((client (slot-value ql-dist 'http-client)))
        (cond
          ((functionp client)
           (funcall client))
          (client
           client)
          (t
           *default-http-client*)))
      *default-http-client*))

(defmethod initialize-instance :after ((dist ql-dist) &rest initargs
                                       &key
                                         uri
                                         force-https
                                         cache-pathname
                                       &allow-other-keys)
  (declare (ignore initargs))
  (let ((uri uri))
    (if (puri:uri-p uri)
        (setf uri (puri:copy-uri uri))
        (setf uri (puri:parse-uri uri)))
    ;; If the URI is already https, set force https to be T
    (when (eql (puri:uri-scheme uri) :https)
      (setf force-https t)
      (setf (ql-dist-force-https dist) force-https))
    ;; If the scheme is not already https, set it to https if necessary.
    (ecase force-https
      ((nil))
      (t
       (setf (puri:uri-scheme uri) :https)))
    ;; Compute the versions uri
    (let* ((path (puri:uri-path uri))
           (file-name (pathname-name path)))
      (setf (ql-dist-versions-uri dist)
            (puri:merge-uris (namestring
                              (merge-pathnames (concatenate 'string file-name "-versions")
                                               path))
                             uri)))
    (unless (uiop:directory-pathname-p cache-pathname)
      (error "cache pathname must name a directory"))
    (ensure-directories-exist cache-pathname)
    (setf (ql-dist-uri dist) uri)))

(defmethod slot-unbound (class (dist ql-dist) (slot-name (eql 'version-uris-map)))
  ;; Fetch the versions file and parse it.
  (let ((versions-pathname (merge-pathnames "versions.txt"
                                            (ql-dist-cache-pathname dist))))
    (http-client-ensure-file-fetched (ql-dist-http-client dist)
                                     versions-pathname
                                     (ql-dist-versions-uri dist))
    (let ((ht (make-hash-table :test 'equal)))
      (with-open-file (s versions-pathname)
        (loop
          (let ((line (read-line s nil :eof)))
            (when (or (eql line :eof) (equal line ""))
              (return))
            (destructuring-bind (version uri)
                (split-sequence #\Space line)
              (setf uri (puri:parse-uri uri))
              (when (ql-dist-force-https dist)
                (setf (puri:uri-scheme uri) :https))
              (setf (gethash version ht) uri)))))
      (setf (slot-value dist 'version-uris-map) ht))))

(defun ql-dist-versions (dist)
  (sort (hash-table-keys (ql-dist-version-uris-map dist))
        #'string<))

(defun ql-dist-version (dist version)
  (ensure-gethash version (ql-dist-version-cache dist)
                  (let ((uri (gethash version (ql-dist-version-uris-map dist))))
                    (unless uri
                      (error "No such version"))
                    (make-instance 'ql-dist-version
                                   :dist dist
                                   :version version
                                   :uri uri))))

(defmacro do-dist-versions ((binding dist) &body body)
  (with-gensyms (v)
    (once-only (dist)
      `(dolist (,v (ql-dist-versions ,dist))
         (let ((,binding (ql-dist-version ,dist ,v)))
           ,@body)))))



(defclass ql-dist-version ()
  ((dist
    :initarg :dist
    :reader ql-dist-version-dist)
   (version
    :initarg :version
    :reader ql-dist-version-version)
   (uri
    :initarg :uri
    :reader ql-dist-version-uri)

   (system-index-uri
    :reader ql-dist-version-system-index-uri)
   (release-index-uri
    :reader ql-dist-version-release-index-uri)

   (release-map
    :reader ql-dist-version-release-map)
   (system-by-name-map
    :reader ql-dist-version-system-by-name-map)
   (system-by-project-map
    :reader ql-dist-version-system-by-project-map)))

(defun ql-dist-version-pathname (dist-version file)
  (merge-pathnames file
                   (uiop:ensure-directory-pathname
                    (merge-pathnames (ql-dist-version-version dist-version)
                                     (ql-dist-cache-pathname
                                      (ql-dist-version-dist dist-version))))))

(defun parse-distinfo-line (line)
  "Parse a single line of a distinfo.txt file."
  (let* ((colon-pos (position #\: line))
         (property (make-keyword (uiop:standard-case-symbol-name
                                  (subseq line 0 colon-pos))))
         (value (subseq line (+ 2 colon-pos))))
    (list property value)))

(defun parse-distinfo (distinfo-pathname)
  "Given a distinfo.txt pathname, return a plist of its contents."
  (let* ((contents (read-file-into-string distinfo-pathname))
         (lines (split-sequence #\Newline contents :remove-empty-subseqs t)))
    (mapcan #'parse-distinfo-line lines)))


(defun populate-ql-dist-version! (dist-version)
  (let ((distinfo-pathname (ql-dist-version-pathname dist-version "distinfo.txt"))
        (dist (ql-dist-version-dist dist-version)))
    (http-client-ensure-file-fetched (ql-dist-http-client dist)
                                     distinfo-pathname
                                     (ql-dist-version-uri dist-version)
                                     :hint :immutable)
    (let ((plist (parse-distinfo distinfo-pathname)))
      (let ((system-index-uri (puri:parse-uri (getf plist :system-index-url)))
            (release-index-uri (puri:parse-uri (getf plist :release-index-url))))
        (when (ql-dist-force-https (ql-dist-version-dist dist-version))
          (setf (puri:uri-scheme system-index-uri) :https
                (puri:uri-scheme release-index-uri) :https))
        (setf (slot-value dist-version 'system-index-uri) system-index-uri
              (slot-value dist-version 'release-index-uri) release-index-uri))))
  dist-version)

(defmethod slot-unbound (class (dist-version ql-dist-version) (slot-name (eql 'system-index-uri)))
  (populate-ql-dist-version! dist-version)
  (ql-dist-version-system-index-uri dist-version))

(defmethod slot-unbound (class (dist-version ql-dist-version) (slot-name (eql 'release-index-uri)))
  (populate-ql-dist-version! dist-version)
  (ql-dist-version-release-index-uri dist-version))


(defun populate-ql-dist-version-releases! (dist-version)
  (let ((ht (make-hash-table :test 'equal))
        (releases-pathname (ql-dist-version-pathname dist-version "releases.txt"))
        (dist (ql-dist-version-dist dist-version)))
    (http-client-ensure-file-fetched (ql-dist-http-client dist)
                                     releases-pathname
                                     (ql-dist-version-release-index-uri dist-version)
                                     :hint :immutable)
    (with-open-file (s releases-pathname)
      (loop
        (let ((line (read-line s nil :eof)))
          (when (eql line :eof)
            (return))
          ;; Skip empty lines and comments
          (unless (or (equal line "")
                      (equal (aref line 0) #\#))
            (destructuring-bind (project uri size file-md5 content-sha1 prefix &rest system-files)
                (split-sequence #\Space line)
              (setf uri (puri:parse-uri uri))
              (when (ql-dist-force-https (ql-dist-version-dist dist-version))
                (setf (puri:uri-scheme uri) :https))
              (setf size (parse-integer size))
              (setf (gethash project ht)
                    (make-instance 'ql-release
                                   :dist-version dist-version
                                   :project project
                                   :uri uri
                                   :size size
                                   :file-md5 file-md5
                                   :content-sha1 content-sha1
                                   :prefix prefix
                                   :system-files system-files)))))))
    (setf (slot-value dist-version 'release-map) ht)))

(defmethod slot-unbound (class (dist-version ql-dist-version) (slot-name (eql 'release-map)))
  (populate-ql-dist-version-releases! dist-version)
  (slot-value dist-version 'release-map))

(defmethod populate-ql-dist-version-systems! (dist-version)
  (let ((ht-by-system-name (make-hash-table :test 'equal))
        (ht-by-project (make-hash-table :test 'equal))
        (systems-pathname (ql-dist-version-pathname dist-version "systems.txt"))
        (dist (ql-dist-version-dist dist-version)))
    (http-client-ensure-file-fetched (ql-dist-http-client dist)
                                     systems-pathname
                                     (ql-dist-version-system-index-uri dist-version)
                                     :hint :immutable)
    (with-open-file (s systems-pathname)
      (loop
        (let ((line (read-line s nil :eof)))
          (when (eql line :eof)
            (return))
          ;; Skip empty lines and comments
          (unless (or (equal line "")
                      (equal (aref line 0) #\#))
            (destructuring-bind (project system-file system-name &rest dependencies)
                (split-sequence #\Space line)
              (let ((system (make-instance 'ql-system
                                           :dist-version dist-version
                                           :project project
                                           :system-file system-file
                                           :system-name system-name
                                           :dependencies dependencies)))
                (setf (gethash system-name ht-by-system-name) system)
                (push system (gethash project ht-by-project))))))))
    (setf (slot-value dist-version 'system-by-name-map) ht-by-system-name
          (slot-value dist-version 'system-by-project-map) ht-by-project)))

(defmethod slot-unbound (class (dist-version ql-dist-version) (slot-name (eql 'system-by-name-map)))
  (populate-ql-dist-version-systems! dist-version)
  (slot-value dist-version 'system-by-name-map))

(defmethod slot-unbound (class (dist-version ql-dist-version) (slot-name (eql 'system-by-project-map)))
  (populate-ql-dist-version-systems! dist-version)
  (slot-value dist-version 'system-by-project-map))

(defun ql-dist-version-release (dist-version project-name
                                &optional (missing-error-p t) missing-value)
  (or (gethash project-name (ql-dist-version-release-map dist-version))
      (and missing-error-p (error "project missing"))
      missing-value))



(defclass ql-release ()
  ((dist-version
    :initarg :dist-version
    :reader ql-release-dist-version)

   (project
    :initarg :project
    :reader ql-release-project)
   (uri
    :initarg :uri
    :reader ql-release-uri)
   (size
    :initarg :size
    :reader ql-release-size)
   (file-md5
    :initarg :file-md5
    :reader ql-release-file-md5)
   (content-sha1
    :initarg :content-sha1
    :reader ql-release-content-sha1)
   (prefix
    :initarg :prefix
    :reader ql-release-prefix)
   (system-files
    :initarg :system-files
    :reader ql-release-system-files)))



(defclass ql-system ()
  ((dist-version
    :initarg :dist-version
    :reader ql-system-dist-version)

   (project
    :initarg :project
    :reader ql-system-project)
   (system-file
    :initarg :system-file
    :reader ql-system-system-file)
   (system-name
    :initarg :system-name
    :reader ql-system-system-name)
   (dependencies
    :initarg :dependencies
    :reader ql-system-dependencies)))
