;;;; QL-CLPI
;;;;
;;;; This software is part of QL-CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:ql-clpi/ql-clpi
    (:nicknames #:ql-clpi)
  (:use #:cl
        #:alexandria
        #:ql-clpi/ql-clpi-index
        #:ql-clpi/ql-dist
        #:ql-clpi/ql-to-clpi)
  (:import-from #:alexandria)
  (:import-from #:clpi)
  (:export #:ql-dist
           #:ql-dist-to-clpi
           #:ql-index
           #:project-version-aliases))

(in-package #:ql-clpi/ql-clpi)

(defun project-version-aliases (project version)
  "Given a PROJECT and a VERSION, return all aliases of that version string."
  (let ((alias-alist (ql-project-version-aliases project)))
    (assoc-value alias-alist version :test 'equal)))
