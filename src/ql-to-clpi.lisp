;;;; Quicklisp Distribution Representation
;;;;
;;;; This software is part of QL-CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:ql-clpi/ql-to-clpi
    (:use #:cl
          #:alexandria
          #:anaphora
          #:ql-clpi/ql-clpi-index
          #:ql-clpi/ql-dist)
  (:import-from #:clpi)
  (:import-from #:puri)
  (:export #:ql-dist-to-clpi))

(in-package #:ql-clpi/ql-to-clpi)

(defvar *appendable-p*)

(defun max-element (list predicate)
  (loop
    :with result := (first list)
    :for el :in (rest list)
    :when (funcall predicate el result)
      :do
         (setf result el)
    :finally
       (return result)))

(defun latest-release-of-project (project)
  (let ((latest-version (max-element (clpi:project-versions project) #'string>)))
    (when latest-version
      (clpi:project-release project latest-version))))

(defun find-or-make-project (index project-name)
  (or (clpi:index-project index project-name nil)
      (let ((project (make-instance (clpi:index-project-class index)
                                    :index index
                                    :name project-name
                                    :version-scheme :date)))
        (when *appendable-p*
          (setf (clpi:project-releases-object-version project) 0))
        (clpi:index-add-project index project)
        project)))

(defun find-or-make-system (index system-name)
  (or (clpi:index-system index system-name nil)
      (let ((system (make-instance (clpi:index-system-class index)
                                   :index index
                                   :name system-name)))
        (clpi:index-add-system index system)
        system)))

(defun ql-version-to-clpi (ql-dist version index)
  (let* ((dv (ql-dist-version ql-dist version))
         (release-map (ql-dist-version-release-map dv))
         (system-by-project-map (ql-dist-version-system-by-project-map dv)))
    (maphash (lambda (project-name ql-release)
               (let* ((uri-string (puri:render-uri (ql-release-uri ql-release) nil))
                      (project (find-or-make-project index project-name))
                      (previous-release (latest-release-of-project project))
                      new-release
                      (system-file-ht (make-hash-table :test 'equal)))
                 (labels ((get-system-file-pathname (name)
                            ;; This madness is becuase releases.txt and
                            ;; systems.txt give different strings for the system
                            ;; files...
                            (aprog1 (find-if (lambda (pn)
                                               (equal (pathname-name pn)
                                                      name))
                                             (ql-release-system-files ql-release))
                              (unless it
                                (error "Unable to determine system file path for system"))))
                          (make-or-get-system-file (pathname-name)
                            (let ((enough-namestring (get-system-file-pathname pathname-name)))
                              (ensure-gethash enough-namestring system-file-ht
                                              (make-instance (clpi:index-system-file-class index)
                                                             :release new-release
                                                             :enough-namestring enough-namestring)))))
                   (when (or (null previous-release)
                             (not (equal (ql-release-file-md5 ql-release)
                                         (clpi:release-md5 previous-release))))
                     ;; This is a release we haven't seen before. Make a new
                     ;; release object.
                     (setf new-release
                           (make-instance (clpi:project-release-class project)
                                          :project project
                                          :version version
                                          :archive-type :tar.gz
                                          :system-files (ql-release-system-files ql-release)
                                          :url uri-string
                                          :size (ql-release-size ql-release)
                                          :md5 (ql-release-file-md5 ql-release)))
                     ;; Create all the system files.
                     (dolist (ql-system (gethash project-name system-by-project-map))
                       (let* ((system (find-or-make-system index (ql-system-system-name ql-system)))
                              (system-file (make-or-get-system-file (ql-system-system-file ql-system)))
                              (system-release
                                (make-instance (clpi:index-system-release-class index)
                                               :name (ql-system-system-name ql-system)
                                               :system-file system-file
                                               :dependencies (ql-system-dependencies ql-system))))
                         (clpi:system-add-system-release system system-release)
                         (setf (clpi:system-primary-project system)
                               (clpi:project-name project))
                         (clpi:system-file-add-system-release system-file system-release)))
                     ;; Add the system files to the release.
                     (dolist (new-system-file (hash-table-values system-file-ht))
                       (clpi:release-add-system-file new-release new-system-file))
                     ;; Add the release to the project.
                     (clpi:project-add-release project new-release)))
                 (when (and previous-release
                            (equal (ql-release-file-md5 ql-release)
                                   (clpi:release-md5 previous-release)))
                   ;; This release exists already from a previous dist. Add an
                   ;; alias to the project.
                   (push version
                         (assoc-value (ql-project-version-aliases project)
                                      (clpi:release-version previous-release) :test 'equal)))))
             release-map)))

(defun ql-dist-to-clpi (ql-dist index &key appendable-p)
  (let ((*appendable-p* appendable-p)
        (latest-version-synced (ql-index-latest-version-synced index))
        (versions (ql-dist-versions ql-dist))
        (modified-p nil))
    (dolist (version versions)
      (unless (and latest-version-synced
                   (string<= version latest-version-synced))
        (ql-version-to-clpi ql-dist version index)
        (setf modified-p t)
        (setf (ql-index-latest-version-synced index) version)))
    modified-p))
