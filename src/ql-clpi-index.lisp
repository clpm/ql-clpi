;;;; Quicklisp CLPI Index
;;;;
;;;; This software is part of QL-CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:ql-clpi/ql-clpi-index
    (:use #:cl)
  (:import-from #:clpi)
  (:export #:ql-index
           #:ql-index-latest-version-synced
           #:ql-project-version-aliases))

(in-package #:ql-clpi/ql-clpi-index)


;; * Index

(defclass ql-index (clpi:index)
  ((latest-version-synced
    :initarg :latest-version-synced
    :accessor ql-index-latest-version-synced)))

(defmethod slot-unbound (class (index ql-index) (slot-name (eql 'latest-version-synced)))
  (clpi:with-open-object-stream (s (clpi:index-object-store index) "ext/ql/latest-version-synced"
                                   :if-does-not-exist nil)
    (setf (ql-index-latest-version-synced index)
          (when s
            (clpi:with-clpi-io-syntax ()
              (read s))))))

(defmethod clpi:index-save :after ((index ql-index))
  (when (slot-boundp index 'latest-version-synced)
    (clpi:with-open-object-stream (s (clpi:index-object-store index) "ext/ql/latest-version-synced"
                                     :direction :output
                                     :if-exists :supersede)
      (clpi:with-clpi-io-syntax ()
        (write (ql-index-latest-version-synced index) :stream s)
        (terpri s)))))

(defmethod clpi:index-project-class ((index ql-index))
  'ql-project)

(defmethod clpi:index-sync :after ((index ql-index))
  (let ((object-store (clpi:index-object-store index)))
    (when (typep object-store 'clpi:dual-object-store)
      (clpi:dual-object-store-update-object object-store "ext/ql/latest-version-synced")
      (dolist (p (clpi:index-projects index))
        (clpi:dual-object-store-update-object object-store
                                              (clpi:project-object-path p "ext/ql/version-aliases"))))))


;; * Projects

(defclass ql-project (clpi:project)
  ((version-aliases
    :initarg :version-aliases
    :accessor ql-project-version-aliases)))

(defmethod slot-unbound (class (project ql-project) (slot-name (eql 'version-aliases)))
  (clpi:with-open-object-stream (s (clpi:index-object-store (clpi:index project))
                                   (clpi:project-object-path project "ext/ql/version-aliases")
                                   :if-does-not-exist nil)
    (setf (ql-project-version-aliases project)
          (when s
            (clpi:with-clpi-io-syntax ()
              (read s))))))

(defmethod clpi:project-save :after ((project ql-project))
  (when (slot-boundp project 'version-aliases)
    (clpi:with-open-object-stream (s (clpi:index-object-store (clpi:index project))
                                     (clpi:project-object-path project "ext/ql/version-aliases")
                                     :direction :output
                                     :if-exists :supersede)
      (clpi:with-clpi-io-syntax ()
        (write (ql-project-version-aliases project) :stream s)
        (terpri s)))))
