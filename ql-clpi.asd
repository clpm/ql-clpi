;;;; QL-CLPI System Definition
;;;;
;;;; This software is part of QL-CLPI. See README.org for more information. See
;;;; LICENSE for license information.

#-:asdf3.2
(error "Requires ASDF >=3.2")

;; Not necessary, but easier to have when using SLIME.
(in-package :asdf-user)

(defsystem #:ql-clpi
  :version "0.0.1"
  :description "A library to convert Quicklisp metadata to CLPI metadata."
  :license "BSD-2-Clause"
  :pathname "src/"
  :class :package-inferred-system
  :depends-on (#:ql-clpi/ql-clpi))
