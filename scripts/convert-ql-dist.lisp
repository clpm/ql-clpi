;;;; Script to convert a QL distribution to a file based CLPI index.
;;;;
;;;; This software is part of QL-CLPI. See README.org for more information. See
;;;; LICENSE for license information.

(in-package #:cl-user)

(require :asdf)

(asdf:load-system :ql-clpi)
(asdf:load-system :clpm-multi-http-client/drakma)

(uiop:define-package #:ql-clpm-scripts/convert-ql-dist
    (:use #:cl
          #:ql-clpi
          #:ql-clpi/ql-dist
          #:ql-clpi/ql-to-clpi))

(in-package #:ql-clpm-scripts/convert-ql-dist)

(defun sync-ql-dist-to-file-index (ql-dist-uri ql-cache-dir clpi-dir
                                   &key http-client
                                     appendable-p)
  (let ((ql-dist (make-instance 'ql-dist
                                :uri ql-dist-uri
                                :cache-pathname ql-cache-dir
                                :http-client http-client))
        (index (make-instance 'ql-clpi:ql-index
                              :object-store
                              (make-instance 'clpi:file-object-store
                                             :root clpi-dir))))
    (ql-dist-to-clpi ql-dist index :appendable-p appendable-p)
    index))

(defun run ()
  (clpi:index-save
   (sync-ql-dist-to-file-index "https://beta.quicklisp.org/dist/quicklisp.txt"
                               "/tmp/clpi/ql/"
                               "/tmp/clpi/file/"
                               :http-client
                               (make-instance 'clpm-multi-http-client/drakma:drakma-client)
                               :appendable-p t)))
(run)
